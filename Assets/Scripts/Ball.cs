﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

    // Balance constants
    private const float minRespawnX = 0.3f;
    private const float maxRespawnX = 0.3f;
    private const float minRespawnY = 0.6f;
    private const float maxRespawnY = 0.6f;
    private const float slowFactor = 2f;

    private Vector3 currentDirection;
    private float timeSinceMove;

	// Use this for initialization
	void Start () {
        timeSinceMove = 0f;

        transform.position = WorldGrid.SnapToGrid(transform.position);

        if (Random.Range(0f, 1f) < 0.5f)
        {
            currentDirection = Vector3.left;
        }
        else
        {
            currentDirection = Vector3.right;
        }
	}
	
	// Update is called once per frame
	void Update () {

    }

    private void FixedUpdate()
    {
        Move();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        SnakeHead hitPlayer = null;
        SnakeBody hitBody = collision.gameObject.GetComponent<SnakeBody>();
        if (hitBody != null)
        {
            hitPlayer = hitBody.head;
        }
        else
        {
            hitPlayer = collision.gameObject.GetComponent<SnakeHead>();
        }
        if (hitPlayer != null)
        {
            hitPlayer.RemoveSegment();
            currentDirection = hitPlayer.playerID * Vector3.left;
            transform.position += currentDirection * WorldGrid.Size;
        }
    }

    private void Move()
    {
        timeSinceMove += Time.fixedDeltaTime;
        // Move the ball if enough time has past.
        if (timeSinceMove >= slowFactor * WorldGrid.MoveInterval)
        {
            timeSinceMove = 0f;
            Vector3 oldPosition = transform.position;
            transform.position += currentDirection * WorldGrid.Size;
        }
        // If the ball has moved offscreen, score a point and reset it's position.
        if (transform.position.x > WorldGrid.maxX)
        {
            ScoreBall(0);
        }
        else if (transform.position.x < WorldGrid.minX)
        {
            ScoreBall(1);
        }
    }

    private void ScoreBall(int playerID)
    {
        ScoreBoard.IncrementScore(playerID);

        transform.position = NewRandomPosition();
        currentDirection = -1 * currentDirection;

        SnakeHead[] snakes = GameObject.FindObjectsOfType<SnakeHead>();
        for (int i = 0; i < snakes.Length; i++)
        {
            if (snakes[i].playerID != playerID)
            {
                snakes[i].GetScoredOn();
                break;
            }
        }
    }

    private Vector3 NewRandomPosition()
    {
        float newX = Random.Range(minRespawnX * WorldGrid.minX, maxRespawnX * WorldGrid.maxX);
        float newY = Random.Range(minRespawnY * WorldGrid.minY, maxRespawnY * WorldGrid.maxY);
        Vector3 newPos = new Vector3(newX, newY, transform.position.z);
        return WorldGrid.SnapToGrid(newPos);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreBoard : MonoBehaviour {

    private static Text scoreBoardText;
    private static Dictionary<int, int> playerScore;

	// Use this for initialization
	void Start () {
        scoreBoardText = GameObject.Find("ScoreBoard").GetComponent<Text>();
        playerScore = new Dictionary<int, int>();
        playerScore[0] = 0;
        playerScore[1] = 0;
    }
	
	// Update is called once per frame
	void Update () {
        scoreBoardText.text = playerScore[0] + "    Score    " + playerScore[1];
	}

    public static void IncrementScore(int playerID)
    {
        if (!playerScore.ContainsKey(playerID))
        {
            playerScore[playerID] = 0;
        }
        playerScore[playerID]++;
    }
}

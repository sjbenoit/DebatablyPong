﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakeHead : MonoBehaviour {

    [SerializeField]
    public int playerID;
    [SerializeField]
    public KeyCode UpButton;
    [SerializeField]
    public KeyCode RightButton;
    [SerializeField]
    public KeyCode DownButton;
    [SerializeField]
    public KeyCode LeftButton;
    [SerializeField]
    public Color playerColour;
    [SerializeField]
    public float spawnX;
    [SerializeField]
    public float spawnY;

    // Balance Constants
    private const int startingLength = 10;
    private const int scoredOnGrowth = 10;
    private const float respawnTime = 5f;

    // Private trackers. Do not set manually!
    private Vector3 currentDirection;
    private bool turnedSinceMove;
    private bool spawned;
    private bool justSpawned;
    private SnakeBody next;
    private float timeSinceMove;

	// Use this for initialization
	void Start () {
        currentDirection = Vector3.up;
        turnedSinceMove = false;
        spawned = true;
        justSpawned = true;
        next = null;
        timeSinceMove = 0f;
        gameObject.GetComponent<SpriteRenderer>().color = playerColour;

        transform.position = WorldGrid.SnapToGrid(transform.position);

        for (int i = 1; i < startingLength; i++)
        {
            AddSegment();
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        // DEV CONTROLS.
        // TODO: REMOVE THESE!
        if (Input.GetKeyUp(KeyCode.R))
        {
            AddSegment();
        }
        else if (Input.GetKeyUp(KeyCode.F))
        {
            RemoveSegment();
        }


        // Player Controls
        if (Input.GetKeyUp(UpButton))
        {
            Turn(Vector3.up);
        }
        else if (Input.GetKeyUp(RightButton))
        {
            Turn(Vector3.right);
        }
        else if (Input.GetKeyUp(DownButton))
        {
            Turn(Vector3.down);
        }
        else if (Input.GetKeyUp(LeftButton))
        {
            Turn(Vector3.left);
        }
    }

    private void FixedUpdate()
    {
        if (spawned)
        {
            Move();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        SnakeHead hitPlayer = null;
        SnakeBody hitBody = collision.gameObject.GetComponent<SnakeBody>();
        if (hitBody != null)
        {
            hitPlayer = hitBody.head;
        }
        else
        {
            hitPlayer = collision.gameObject.GetComponent<SnakeHead>();
        }
        if (hitPlayer != null && !justSpawned)
        {
            StartCoroutine(Respawn());
        }
    }

    private void Turn(Vector3 newDirection)
    {
        if (newDirection != -1 * currentDirection && !turnedSinceMove)
        {
            currentDirection = newDirection;
            turnedSinceMove = true;
        }
    }

    private void Move()
    {
        timeSinceMove += Time.fixedDeltaTime;
        if (timeSinceMove >= WorldGrid.MoveInterval)
        {
            timeSinceMove = 0f;
            Vector3 oldPosition = transform.position;
            transform.position += currentDirection * WorldGrid.Size;
            turnedSinceMove = false;
            justSpawned = false;

            if (next != null)
            {
                next.Move(oldPosition);
            }
        }
    }

    public IEnumerator Respawn()
    {
        justSpawned = true;
        spawned = false;
        // Remove this snake
        while (next != null)
        {
            RemoveSegment();
        }
        // Hide waaaaayyyyyyyyy off screen
        transform.position = new Vector3(WorldGrid.maxX * 10, playerID * 10f, 0f);

        // Wait for a while
        yield return new WaitForSeconds(respawnTime);

        // Reset position
        Vector3 spawnPosition = new Vector3(spawnX, spawnY, 0f);
        transform.position = WorldGrid.SnapToGrid(spawnPosition);

        // Reset direction
        currentDirection = Vector3.up;

        // Reset Length
        for (int i = 1; i < startingLength; i++)
        {
            AddSegment();
        }
        spawned = true;
    }

    public void AddSegment()
    {
        if (next == null)
        {
            GameObject newSegment = Instantiate(Resources.Load("Prefabs/SnakeBody", typeof(GameObject))) as GameObject;
            newSegment.transform.position = transform.position;
            newSegment.GetComponent<SpriteRenderer>().color = playerColour;
            next = newSegment.GetComponent<SnakeBody>();
            next.head = this;
        }
        else
        {
            next.AddSegment(playerColour);
        }
    }

    public void RemoveSegment()
    {
        if (next != null)
        {
            next = next.RemoveSegment();
        }
        else
        {
            StartCoroutine(Respawn());
        }
    }

    public void GetScoredOn()
    {
        for (int i = 0; i < scoredOnGrowth; i++)
        {
            AddSegment();
        }
    }
}

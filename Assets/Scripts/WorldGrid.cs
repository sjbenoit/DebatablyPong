﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldGrid : MonoBehaviour {

    public static float Size = 0.2f;
    public static float MoveInterval = 0.1f;
    public static float maxX = 11f;
    public static float minX = -11f;
    public static float maxY = 5f;
    public static float minY = -5f;

    private static Grid worldGrid;

    // Use this for initialization
    void OnEnable () {
        worldGrid = GetComponent<Grid>();
        worldGrid.cellSize = new Vector3(Size, Size, 0f);
	}

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update () {
		
	}

    public static Vector3 SnapToGrid(Vector3 position)
    {
        return worldGrid.CellToWorld(worldGrid.WorldToCell(position));
    }
}

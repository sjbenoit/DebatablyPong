﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakeBody : MonoBehaviour {

    public SnakeHead head;
    private SnakeBody next;
    private bool justSpawned;

    // Use this for initialization
    void Start () {
        justSpawned = true;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Move (Vector3 newPosition)
    {
        if (justSpawned)
        {
            justSpawned = false;
            return;
        }
        Vector3 oldPosition = transform.position;
        transform.position = newPosition;
        if (next != null)
        {
            next.Move(oldPosition);
        }
    }

    public void AddSegment(Color colour)
    {
        if (next == null)
        {
            GameObject newSegment = Instantiate(Resources.Load("Prefabs/SnakeBody", typeof(GameObject))) as GameObject;
            newSegment.transform.position = transform.position;
            newSegment.GetComponent<SpriteRenderer>().color = colour;
            next = newSegment.GetComponent<SnakeBody>();
            next.head = head;
        }
        else
        {
            next.AddSegment(colour);
        }
    }

    public SnakeBody RemoveSegment()
    {
        if (next == null)
        {
            // Destroy this segment
            Destroy(gameObject);
            return null;
        }
        else
        {
            next = next.RemoveSegment();
            return this;
        }
    }
}
